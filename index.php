<?php 
    session_start();

    $_SESSION['path'] = dirname(__FILE__);
    $pathIndex = $_SESSION['path'];

    session_write_close();
?>
<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Latest compiled and minified CSS  -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

    
    <link rel="stylesheet" href="/sitoweb/HOME/style.css">
    <link rel="stylesheet" href="/sitoweb/TEMPL/global.css">


</head>

<body>

    <header> <?php include_once $pathIndex . '/TEMPL/HEADER/header.php'; ?> </header>

    <br>

    <div class="container">

        <div class="row">

            <div class="col-6" style="align-items: center;">
                <br>
                <h3>About me</h3>

                <div class="immagine" style="margin: 0 auto;">
                    <img src="/sitoweb/IMG/maurizio.png" alt="">
                </div>
                
            </div>

            <div class="col-6 contenuto">
                <p>Mi chiamo Maurizio Dello Preite, frequento l'IIS Marconi Pieralisi a Jesi. Studio informatica, che è sempre stato il mio principale hobby. In questo blog illustrerò quali sono le mie passioni, il mio percorso fino adesso e le mie ambizioni future.</p>
            </div>

        </div>

        <br>

    </div>
    
    <footer> <?php include_once $pathIndex . '/TEMPL/FOOTER/footer.php'; ?> </footer>

    <script>

        var elemento = document.getElementById("HOME");
        elemento.classList.add("underLine");

    </script>

    

</body>

<!-- Latest compiled JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>

</html>