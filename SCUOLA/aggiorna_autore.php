<?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') 
    {
        session_start();
        unset($_SESSION['autore']);
        // Recupera il nome dell'autore inviato tramite POST
        $autore = $_POST['autore'];
        
        // Esempio: Salva il nome dell'autore in una variabile di sessione
        
        $_SESSION['autore'] = $autore;
    }
?>
