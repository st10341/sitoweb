<?php 
    session_start();

    $pathIndex = $_SESSION['path'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Latest compiled and minified CSS  -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link rel="stylesheet" href="scuola.css">
    <link rel="stylesheet" href="/sitoweb/TEMPL/global.css">

    <script>
        // Funzione per gestire il click sui contenitori
        function handleContainerClick(autore) {
            var xhr = new XMLHttpRequest();
            xhr.open('POST', 'aggiorna_autore.php', true);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    console.log(autore);
                }
            };
            xhr.send('autore=' + autore);
            
        }
    </script>
</head>
<body>
    

    <header> <?PHP include_once $pathIndex . '/TEMPL/HEADER/header.php'; ?> </header>

    <br>
    <br>
    <br>

    <div id="italiano">

        <div class="container">

            <h2>Italiano</h2>
            <h6>Autori trattati durante l'anno scolastico</h6>
            <hr>

            <div class="row">

                <div class="me-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Giovanni Verga')">
                    <img src="/sitoweb/IMG/verga.jpeg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Giovanni Verga <br> 1840 - 1922
                                <input type="text" name="autore" value="Giovanni Verga" hidden>
                            </p>
                        </div>

                    </div>

                </div>


                <div class="col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Giovanni Pascoli')">

                    <img src="/sitoweb/IMG/pascoli.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Giovanni Pascoli <br> 1855 - 1912
                            </p>
                        </div>

                    </div>

                </div>

                <div class="ms-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Gabriele D\'Annunzio')">

                    <img src="/sitoweb/IMG/dannunzio.webp">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Gabriele D'Annunzio <br> 1863 - 1938
                            </p>
                        </div>

                    </div>

                </div>

            </div>

            <br>

            <div class="row">

                <div class="me-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Luigi Pirandello')">

                    <img src="/sitoweb/IMG/pirandello.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Luigi Pirandello <br> 1867 - 1936
                            </p>
                        </div>

                    </div>

                </div>


                <div class="col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Italo Svevo')">

                    <img src="/sitoweb/IMG/svevo.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Italo Svevo <br> 1861 - 1928
                            </p>
                        </div>

                    </div>

                </div>

                <div class="ms-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Giuseppe Ungaretti')">

                    <img src="/sitoweb/IMG/ungaretti.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Giuseppe Ungaretti <br> 1888 - 1970
                            </p>
                        </div>

                    </div>

                </div>

            </div>

            <br>

            <div class="row">

                <div class="me-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Salvatore Quasimodo')">

                    <img src="/sitoweb/IMG/quasimodo.jpg">
                    <div class="sfondo-hover">
                        
                        <div class="testo-nascosto">
                            <p class="testo">
                                Salvatore Quasimodo <br> 1901 - 1968
                            </p>
                        </div>

                    </div>

                </div>


                <div class="col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Eugenio Montale')">

                    <img src="/sitoweb/IMG/montale.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Eugenio Montale <br> 1896 - 1981       
                            </p>
                        </div>

                    </div>

                </div>

                <div class="ms-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Primo Levi')">

                    <img src="/sitoweb/IMG/levi.jpeg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Primo Levi <br> 1919 - 1987
                            </p>
                        </div>

                    </div>

                </div>

            </div>

        </div>
        
    </div>
    

    <br>
    <br>
    <br>


    <div id="storia">
        <div class="container">
            <h2>Storia</h2>
            <h6>Argomenti trattati durante l'anno scolastico</h6>
            <hr>
            <div class="row">

                <div class="me-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Catena di montaggio')">

                    <img src="/sitoweb/IMG/catenaMontaggio.webp">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Catena di montaggio fordista <br> 1913
                            </p>
                        </div>

                    </div>

                </div>


                <div class="col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Guglielmo II')">

                    <img src="/sitoweb/IMG/guglielmoII.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                La germania di Guglielmo II
                            </p>
                        </div>

                    </div>

                </div>

                <div class="ms-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Sviluppo econimico russo')">

                    <img src="/sitoweb/IMG/ferroviaTransiberiana.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Sviluppo economico russo <br> XIX secolo
                            </p>
                        </div>

                    </div>

                </div>

            </div>

            <br>

            <div class="row">

                <div class="me-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Giovanni Giolitti')">

                    <img src="/sitoweb/IMG/giovanniGiolitti.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Giovanni Giolitti <br> 1842 - 1928
                            </p>
                        </div>

                    </div>

                </div>


                <div class="col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Sviluppo industriale in Italia')">

                    <img src="/sitoweb/IMG/sviluppoItalia.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Sviluppo industriale in italia <br> XIX secolo
                            </p>
                        </div>

                    </div>

                </div>

            </div>

            <br>

            <div class="row">

                <div class="me-5 col-12 bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Prima Guerra Mondiale')">

                    <img src="/sitoweb/IMG/primaGuerra.png">
                    <div class="sfondo-hover">
                        
                        <div class="testo-nascosto">
                            <p class="testo">
                            Prima guerra mondiale <br> 1914 - 1919
                            </p>
                        </div>

                    </div>

                </div>

            </div>

            <br>

            <div class="row">

                <div class="me-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Trattati di pace')">

                    <img src="/sitoweb/IMG/trattatoVersailles.jpg">
                    <div class="sfondo-hover">
                        
                        <div class="testo-nascosto">
                            <p class="testo">
                                Trattati di pace <br> 18 gennaio 1919
                            </p>
                        </div>

                    </div>

                </div>


                <div class="col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('NEP')">

                    <img src="/sitoweb/IMG/nep.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                NEP (Nuova Politica Economica) <br> 1921 - 1928
                            </p>
                        </div>

                    </div>

                </div>

                <div class="ms-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Movimento fascista')">

                    <img src="/sitoweb/IMG/pnf.png">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Il movimento fascista <br> 23 marzo 1919
                            </p>
                        </div>

                    </div>

                </div>

            </div>

            <br>

            <div class="row">

                <div class="me-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Delitto Matteotti')">

                    <img src="/sitoweb/IMG/matteotti.png">
                    <div class="sfondo-hover">
                        
                        <div class="testo-nascosto">
                            <p class="testo">
                                Delitto Matteotti <br> 10 giugno 1924
                            </p>
                        </div>

                    </div>

                </div>


                <div class="col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Leggi razziali')">

                    <img src="/sitoweb/IMG/leggiRazziali.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Leggi Razziali <br> settembre 1938 - giugno 1939
                            </p>
                        </div>

                    </div>

                </div>

                <div class="ms-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Wall Street')">

                    <img src="/sitoweb/IMG/wallStreet.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Il crollo di Wall Street <br> 24 ottobre 1929
                            </p>
                        </div>

                    </div>

                </div>

            </div>

            <br>

            <div class="row">

                <div class="me-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Roosevelt')">

                    <img src="/sitoweb/IMG/roosevelt.jpg">
                    <div class="sfondo-hover">
                        
                        <div class="testo-nascosto">
                            <p class="testo">
                                New Deal di Roosevelt <br> 1932
                            </p>
                        </div>

                    </div>

                </div>


                <div class="col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('URSS')">

                    <img src="/sitoweb/IMG/urss.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Unione delle Repubbliche Sociliste Sovietiche <br> 30 dicembre 1922
                            </p>
                        </div>

                    </div>

                </div>

                <div class="ms-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Germania nazista')">

                    <img src="/sitoweb/IMG/hitler.jpg">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                La germania nazista
                            </p>
                        </div>

                    </div>

                </div>

            </div>

            <br>

            <div class="row">

                <div class="me-5 col-12 bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Seconda Guerra Mondiale')">

                    <img src="/sitoweb/IMG/secondaGuerra.png">
                    <div class="sfondo-hover">
                        
                        <div class="testo-nascosto">
                            <p class="testo">
                                Seconda Guerra Mondiale <br> 1939 - 1945
                            </p>
                        </div>

                    </div>

                </div>

            </div>

            <br>

            <div class="row">

                <div class="me-5 col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Hiroshima')">

                    <img src="/sitoweb/IMG/hiroshima.jpg">
                    <div class="sfondo-hover">
                        
                        <div class="testo-nascosto">
                            <p class="testo">
                                Bomba atomica su Hiroshima <br> 6 agosto 1945
                            </p>
                        </div>

                    </div>

                </div>
                

                <div class="col bordo p-0 contenitore bg-blue" onclick="handleContainerClick('Nagasaki')">

                    <img src="/sitoweb/IMG/nagasaki.jpg">
                    <div class="sfondo-hover">
                        
                        <div class="testo-nascosto">
                            <p class="testo">
                                Bomba atomica su Nagasaki <br> 9 agosto 1945
                            </p>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <br>
    <br>
    <br>

    <div id="edCivica">
        <div class="container">
            <h2>Educazione civica</h2>
            <h6>Argomenti e progetti svolti</h6>
            <hr>
            <br>
            <h2 style="text-align: center;">Profiling</h2>
            <div class="row">

                <div class="col bordo p-0 contenitore2 bg-blue">

                    <img src="/sitoweb/IMG/profiling.png">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                Il profiling
                            </p>
                        </div>

                    </div>

                </div>

                
            </div>

            <br>

            <div class="row">
                <div class="col">
                    <p style="text-align: justify;">
                        Un argomento di educazione civica che abbiamo trattato è quello del profiling. Nello specifico ognuno di noi ha realizzato una presentazione in cui viene spiegato inanzi tutto cos'è il profiling,
                        indicando i rischi e il funzionamento e successivamente è stato somministrato per due volte un questionario ad un gruppo di persone, la prima volta senza che avessero letto la presentazione,
                        la seconda volta dopo la lettura del documento. <br> 
                        Di seguito c'è la presentazione con il lavoro svolto.
                    </p>
                    
                </div>
            </div>

            <br>

            <div class="row">
                <iframe src="/sitoweb/TEMPL/profiling.pdf" style="height: 650px;" frameborder="0"></iframe>
            </div>

            <br>
            <br>
            <br>
            
            
            <h2 style="text-align: center;">La guerra</h2>

            <div class="row">
                <div class="col bordo p-0 contenitore2 bg-blue">

                    <img src="/sitoweb/IMG/guerra.png">
                    <div class="sfondo-hover">

                        <div class="testo-nascosto">
                            <p class="testo">
                                La guerra
                            </p>
                        </div>

                    </div>

                </div>
            </div>

            <br>

            <div class="row">
                <div class="col">
                    <p style="text-align: justify;">
                        Un'altro argomento trattato di educazione civica è la guerra, soffermandoci a riflettere sui conflitti che stanno succedendo al giorno d'oggi e provando ad immedesimarci nella figura
                        di un leader mondiale che deve convincere più persone possibili che bisogna trovare un'altrenativa alla guerra. Di seguito la presentazione da me svolta.
                    </p>
                </div>
            </div>

            <br>

            <div class="row">
                <iframe src="/sitoweb/TEMPL/guerra.pdf" style="height: 700px;" frameborder="0"></iframe>
            </div>

        </div>
    </div>
    
    <div class="overlay hidden"></div>
    <div class="divInfo hidden">

        <div id="close">
            <img src="/sitoweb/IMG/close.png" onclick="aggiornaPagina()">
        </div>

        <br>
        <br>

        <?php if(isset($_SESSION['autore'])): ?>

            <strong> <?php echo $_SESSION['autore']; ?> </strong>

            <?php if($_SESSION['autore'] == "Giovanni Verga"): ?>

                <p style="margin: 0;">Vita:</p>
                <ul>
                    <li>Nasce a catania nel 1840;</li>
                    <li>Nel 1872 si trasferisce a Milano;</li>
                    <li>Nel 1874 pubblicò Nedda;</li>
                    <li>Nel 1878 scrisse Rosso Malpelo;</li>
                    <li>Nel 1892 torna a Catania;</li>
                    <li>Morì nel 1922.</li>
                </ul>

                <p style="margin: 0;">Scritti più importanti:</p>
                <ul>
                    <li>Nedda</li>
                    <li>Rosso Malpelo</li>
                    <li>La Roba</li>
                    <li>Mastro-don Gesualdo</li>
                    <li>I Malavoglia</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Giovanni Pascoli"): ?>

                <p style="margin: 0;">Vita:</p>
                <ul>
                    <li>Nasce a San Mauro di Romagna il 31 dicembre 1855;</li>
                    <li>Il 10 agosto 1867 il padre Ruggero venne fucilato;</li>
                    <li>Pascoli pronuncia La grande proletaria si è mossa prima di morire;</li>
                    <li>Muore a Bologna il 6 aprile 1912;</li>
                </ul>

                <p style="margin: 0;">Scritti più importanti:</p>
                <ul>
                    <li>La poetica del fanciullino</li>
                    <li>Myricae</li>
                    <li>Lavandare</li>
                    <li>X Agosto</li>
                    <li>Temporale</li>
                    <li>Lampo</li>
                    <li>Tuono</li>
                    <li>Novembre</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Gabriele D'Annunzio"): ?>

                <p style="margin: 0;">Vita:</p>
                <ul>
                    <li>Nasce a <span style="font-weight: bold;">Pescara</span> il 12 marzo 1863;</li>
                    <li>Nel 1915 si schiera tra gli interventisti;</li>
                    <li>Dopo un'incidente aereo, compone le prose del <span style="font-weight: bold;">Notturno</span>;</li>
                    <li>Muore a Bologna il 6 aprile 1912;</li>
                </ul>

                <p style="margin: 0;">Scritti più importanti:</p>
                <ul>
                    <li>Pastori</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Luigi Pirandello"): ?>

                <p style="margin: 0;">Vita:</p>
                <ul>
                    <li>Nasce in <span style="font-weight: bold;">Sicilia</span> nel 1867;</li>
                    <li>Ha un rapporto conflittuale con il padre;</li>
                    <li>Studia Lettere a <span style="font-weight: bold;">Roma</span>;</li>
                    <li>Nel 1894 sposa Maria Antonietta Portulano su richiesta della famiglia;</li>
                    <li>Nel 1904 pubblica il <span style="font-weight: bold;">fu Mattia Pascal;</span></li>
                    <li>Nel 1921 pubblica <span style="font-weight: bold;">Sei personaggi in cerca d'autore;</span></li>
                    <li>Nel 1925 pubblica <span style="font-weight: bold;">Uno, nessuno e centomila;</span></li>
                    <li>Nel 1924 si iscrive al partito fascista;</li>
                    <li>Nel 1934 vince il premio Nobel per la Letteratura;</li>
                    <li>Muore a Roma nel dicembre 1936.</li>
                </ul>

                <p style="margin: 0;">Scritti più importanti:</p>
                <ul>
                    <li>La maschera;</li>
                    <li>L'Umorismo;</li>
                    <li>Il fu Mattia Pascal;</li>
                    <li>Quaderni di Serafino Gubbio;</li>
                    <li>Uno, nessuno e centomila;</li>
                    <li>La patente;</li>
                    <li>Il treno ha fischiato;</li>
                    <li>Tu ridi;</li>
                    <li>Teatro nel teatro;</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Italo Svevo"): ?>

                <p style="margin: 0;">Vita:</p>
                <ul>
                    <li>Il suo vero nome è <span style="font-weight: bold;">Ettore Schmitz;</span></li>
                    <li>Nasce a trieste nel 1861;</li>
                    <li>Nel 1892 muore il padre e si sposerà quattro anni dopo con Livia Veneziani;</li>
                    <li>Nel 1899 abbandona per un po' la letteratura;</li>
                    <li>Nel 1923 fece uscire <span style="font-weight: bold;">La coscienza di Zeno;</span></li>
                    <li>Muore nel 1928.</li>
                </ul>

                <p style="margin: 0;">Scritti più importanti:</p>
                <ul>
                    <li>La coscienza di Zeno;</li>
                    <ul>
                        <li>Prefazione;</li>
                        <li>Il fumo;</li>
                        <li>La morte del padre;</li>
                        <li>Il matrimonio, la moglie e l'amante;</li>
                        <li>Zeno e il suo antagonista.</li>
                    </ul>
                </ul>

            <?php elseif($_SESSION['autore'] == "Giuseppe Ungaretti"): ?>

                <p style="margin: 0;">Vita:</p>
                <ul>
                    <li>Nado ad Alessandria d'Egitto;</li>
                    <li>Scrisse molti testi e poesie durante la Prima Guerra Mondiale.</li>
                </ul>

                <p style="margin: 0;">Scritti più importanti:</p>
                <ul>
                    <li>Veglia;</li>
                    <li>Fratelli;</li>
                    <li>Sono una creatura;</li>
                    <li>San Martino del carso;</li>
                    <li>Soldati.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Salvatore Quasimodo"): ?>

                <p style="margin: 0;">Vita:</p>
                <ul>
                    <li>Nasce in Sicilia nell'agosto del 1901;</li>
                    <li>Nel 1919 decise di trasferirsi a Roma per studiare ingegneria;</li>
                    <li>Nel 1959 vinse il premio Nobel per la letteratura.</li>
                </ul>

                <p style="margin: 0;">Scritti più importanti:</p>
                <ul>
                    <li>Ed è subito sera;</li>
                    <li>Milano, agosto 1943;</li>
                    <li>Alle fronde dei salici;</li>
                    <li>Uomo del mio tempo.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Eugenio Montale"): ?>

                <p style="margin: 0;">Vita:</p>
                <ul>
                    <li>Nasce a Genova in una famiglia borghese;</li>
                    <li>Si diploma ragioniere;</li>
                    <li>Nel 1935 esce <span style="font-weight: bold;">Ossi si seppia;</span></li>
                    <li>Ricevette il premio Nobel;</li>
                    <li>Muore a 85 anni.</li>
                </ul>

                <p style="margin: 0;">Scritti più importanti:</p>
                <ul>
                    <li>Non chiederci la parola;</li>
                    <li>Mareggiare pallido e assorto;</li>
                    <li>Spesso il male di vivere ho incontrato.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Primo Levi"): ?>

                <p style="margin: 0;">Vita:</p>
                <ul>
                    <li>Nasce a Torino nel 1919 da una famiglia ebraica;</li>
                    <li>Nel 1938 vengono proclamate le leggi razziali;</li>
                    <li>Il <span style="font-weight: bold;">20 febbraio 1944</span> viene portato ad <span style="font-weight: bold;">Aushwitz;</span></li>
                    <li>Nel Lager lavora come chimico;</li>
                    <li>Il 27 gennaio 1945 le truppe sovietiche liberarono Aushwitz;</li>
                    <li>L'11 aprile 1987 <span style="font-weight: bold;">si suicida.</span></li>
                </ul>

                <p style="margin: 0;">Scritti più importanti:</p>
                <ul>
                    <li>Se questo è un uomo. - 1947</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Catena di montaggio"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Età dell'acciaio, dell'elettricità e del motore a scoppio;</li>
                    <li>Realizzazione della catena di montaggio fordista (Henry Ford);</li>
                    <li>Macchina da scrivere e telefono;</li>
                    <li>Alienazione con Taylor.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Guglielmo II"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>La Germania passa da una politica di equilibrio a una politica di potenza;</li>
                    <li>Si è tenuta fuori dall'ondata colonizzatrice.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Sviluppo econimico russo"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Nel 1904 nacque la <span style="font-weight: bold;">ferrovia Transiberiana;</span></li>
                    <li>Potere interamente nelle mani dello zar;</li>
                    <li>Operai sottopagati e contadini poveri e malnutriti;</li>
                    <li>Nicola II fece sparare alla folla che protestava.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Giovanni Giolitti"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Riconobbe ai lavoratori il diritto di scioperare;</li>
                    <li>Età giolittiana dal 1903 al 1914;</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Sviluppo industriale in Italia"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Nuove infrastrutture;</li>
                    <li>Sviluppo industria delle automobili (Lancia, Fiat e Alfa Romeo);</li>
                    <li>Divario nord e sud;</li>
                    <ul>
                        <li>Torino, Genova e Milano veniva considerato il triangolo industriale;</li>
                        <li>La Sardegna era la regione messa peggio;</li>
                        <li>Molta criminalità;</li>
                        <li>Analfabetismo;</li>
                        <li>Rimesse degli emigranti.</li>
                    </ul>
                </ul>

            <?php elseif($_SESSION['autore'] == "Prima Guerra Mondiale"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Tutto iniziò dall'assassinio di <span style="font-weight: bold;">Francesco Ferdinando d'Asburgo;</span></li>
                    <li>Triplice alleanza - scopo difensivo</li>
                    <ul>
                        <li>Austria-Ungheria;</li>
                        <li>Germania;</li>
                        <li>Italia</li>
                    </ul>
                    <li>Triplice intesa</li>
                    <ul>
                        <li>Russia;</li>
                        <li>Francia;</li>
                        <li>Gran Bretagna.</li>
                    </ul>
                    <li>Da guerra di movimento a guerra di posizione;</li>
                    <li>Benito Mussolini;</li>
                    <li>Armi utilizzate:</li>
                    <ul>
                        <li>U-Boot tedeschi;</li>
                        <li>Armi chimiche come l'iprite;</li>
                        <li>Carri armati inglesi</li>
                        <li>Cannone La grande Berta tedesco;</li>
                        <li>Maschera antigas.</li>
                    </ul>
                    <li>Mercato nero;</li>
                    <li>Strafexpedition (Spedizione punitiva per punire il passaggio dell'Italia dall'alleanza all'intesa.);</li>
                    <li>Il <span style="font-weight: bold;">6 aprile 1917</span> gli USA entrano in guerra;</li>                    
                    <li>Ragazzi del '99.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Trattati di pace"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Il presidente Wilson aveva esposto un <span style="font-weight: bold;">programma articolato in 14 punti;</span></li>
                    <li>Germania colpevole delle perdite e dei danni causati;</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "NEP"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Partito socialista e Partito marxista;</li>
                    <li>Decreto di pace e decreto sulla terra;</li>
                    <li>Nel 1918 venne dato l'ordine di giustiziare <span style="font-weight: bold;">Nicola II;</span></li>
                    <li>Con la NEP Lenin voleva reinserire la Russia nel sistema internazionale.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Movimento fascita"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Il 23 marzo 1919 furono fondati i fasci di combattimento da <span style="font-weight: bold;">Benito Mussolini;</span></li>
                    <li>Tra il 27 e 28 ottobre 1922 le camicie nere marciarono su Roma;</li>
                    <li>Il re Emanuele III appoggiava Mussolini;</li>
                    <li>Il governo di Mussolini venne approvato attribuendogli tutti i poteri per 12 mesi.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Delitto Matteotti"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Il <span style="font-weight: bold;">30 maggio 1924</span> il leader del PSU <span style="font-weight: bold;"> Giacomo Matteotti</span> denunciò gli imbrogli dei fascisti durante le votazioni;</li>
                    <li>Il <span style="font-weight: bold;">10 giugno </span>venne rapito da una squadra fascista e il suo corpo venne ritrovato due mesi dopo vicino Roma;</li>
                    <li>Iniziarono molte manifestazioni contro il fascismo;</li>
                    <li>Mussolini il <span style="font-weight: bold;">3 gennaio 1925</span> si assunse l'intera colpa del delitto.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Leggi razziali"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Da settembre 1938 a giugno 1939 il regime fascista promulgò le <span style="font-weight: bold;">leggi per la difesa della razza:</span></li>
                    <ul>
                        <li>Divieto di matrimoni misti tra ariani ed ebrei;</li>
                        <li>Licenziamento degli ebrei dai luoghi pubblici;</li>
                        <li>Divieto di possedere aziende sopra un certo valore.</li>
                    </ul>
                </ul>

            <?php elseif($_SESSION['autore'] == "Wall Street"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Le persone chiedevano prestiti per acquistare azioni che rivendevano ad un prezzo più alto guadagnandoci;</li>
                    <li>Tra il 24 e il 29 ottobre furono messe in vendita 16 milioni di azioni facendo crollare <span style="font-weight: bold;">l'indice medio del valore dei titoli:</span></li>
                    <ul>
                        <li>Le banche bloccarono i prestiti;</li>
                        <li>Molti fallimenti dei possessori di piccoli e medi capitali;</li>
                        <li>9000 banche falliscono.</li>
                    </ul>
                </ul>

            <?php elseif($_SESSION['autore'] == "Roosevelt"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Nel 1932 <span style="font-weight: bold;">Franklin Delano Roosevelt </span>diede inizio al New Deal, cioè al nuovo corso;</li>
                    <li>Roosevelt era un abile comunicatore via radio;</li>
                    <li>Dal 1933 ci furono molti cambiamenti nel paese come:</li>
                    <ul>
                        <li>Salari minimi dei lavoratori;</li>
                        <li>Riduzione degli orari di lavoro;</li>
                        <li>Aumento delle imposte sui redditi più alti.</li>
                    </ul>
                </ul>

            <?php elseif($_SESSION['autore'] == "URSS"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Il <span style="font-weight: bold;">30 dicembre 1922 </span>il X congresso dei soviet fece nascere l'<span style="font-weight: bold;">Unione delle Repubbliche Socialiste Sovietiche;</span></li>
                    <li>Stalin iniziò a farsi notare;</li>
                    <li>Dal 1929 iniziò il <span style="font-weight: bold;">sequestro delle proprietà dai kulaki e la collettivizzazione forzata;</span></li>
                    <li>L'Unione Sovietica creò i <span style="font-weight: bold;">Gulag, cioè campi di concentramento e lavoro forzato.</span></li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Germania nazista"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>C'erano le SA, che eerano le squadre d'assalto, e le SS, le guardie del corpo di Hitler;</li>
                    <li>Hitler scrisse nel 1925 il <span style="font-weight: bold;">Mein Kampf;</span></li>
                    <li>Grazie a Joseph Goebbels il partito cominciò a farsi strada nella società tedesca;</li>
                    <li>Il 30 gennaio 1933 Hitler fu incaricato di andare al potere;</li>
                    <li>Dopo la morte di Hindenburg, Hitler fece nascere il <span style="font-weight: bold;">Nuovo Reich</span> e assunse la carica di <span style="font-weight: bold;">Fuhrer;</span></li>
                    <li>Con le Leggi di Norimberga del 1935 gli ebrei vennero privati dei diritti civici e politici;</li>
                    <li>Tra il 9 e il 10 novembre 1938 durante la notte dei cristalli molte vetrine di negozi ebrei furono distrutte;</li>
                    <li>Diffusione dei campi di concentramento.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Seconda Guerra Mondiale"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Nel 1938 Hitler invade l'Austria per unificare il Terzo Reich;</li>
                    <li>L'inghilterra attua una politica attendista e chiede a Mussolini di fare da mediatore;</li>
                    <li>Nel 1939 Hitler invade la Polonia;</li>
                    <li>Inghilterra, Francia, Olanda e Romania decidono di intervenire;</li>
                    <li>Mussolini decide di invadere l'Albania;</li>
                    <li>Nel maggio del 1939 Italia e Germania firmano il <span style="font-weight: bold;">Patto d'acciaio;</span></li>
                    <li>Russia e Germania firmarono il patto chiamato <span style="font-weight: bold;">Molotov e von Ribbentrop;</span></li>
                    <li><span style="font-weight: bold;">Il 1° settembre del 1939</span> la Germania invade la Polonia;</li>
                    <li>Il <span style="font-weight: bold;"> 10 giugno 1940</span> l'Italia dichiara guerra alla Francia;</li>
                    <li>L'Italia non era pronta per entrare in guerra e inizia a nascere un senso di sfiducia verso il duce;</li>
                    <li>Mussolini venne catturato e imprigionato sul Gran Sasso;</li>
                    <li>Poco dopo Mussolini venne liberato e costituì la repubblica di Salò;</li>
                    <li>L'italia era spaccata in due e gli alleati fecero di tutto per liberarla dai tedeschi;</li>
                    <li>Il 29 aprile i tedeschi firmano la resa;</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Hiroshima"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>La bomba atomica viene considerato il primo peccato della fisica;</li>
                    <li>Bomba all'uranio esplosa alla fine della Seconda Guerra Mondiale il <span style="font-weight: bold;">6 agosto 1945;</span></li>
                    <li>Ha distrutto il 70% degli edifici ed ucciso 140.000 persone;</li>
                    <li>Provocò un aumento dei tassi di cancro, laucemie ed altre malattie croniche.</li>
                </ul>

            <?php elseif($_SESSION['autore'] == "Nagasaki"): ?>

                <p style="margin: 0;">Avvenimenti principali:</p>
                <ul>
                    <li>Avvenuta tre giorni dopo rispetto a Hiroshima, precisamente il <span style="font-weight: bold;">9 agosto 1945;</span></li>
                    <li>Provocò lo stesso quantitativo di danni della prima bomba e 74.000 morti.</li>
                </ul>

            <?php endif; ?>

        <?php endif; ?>
    </div>

    <script>
        function aggiornaPagina(){
            location.reload();
        }

        document.querySelectorAll('.contenitore').forEach(function(element) {
            element.addEventListener('click', function() {
                var overlay = document.querySelector('.overlay');
                var divInfo = document.querySelector('.divInfo');
                
                // Toggle the visibility of the overlay and divInfo
                overlay.classList.toggle('hidden');
                divInfo.classList.toggle('hidden');
            });
        });


    </script>

    <br>
    <br>

    <footer> <?php include_once $pathIndex . '/TEMPL/FOOTER/footer.php'; ?> </footer>

    <script>

        var elemento = document.getElementById("PS");
        elemento.classList.add("underLine");

    </script>

</body>
</html>