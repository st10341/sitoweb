<?php 
    session_start();

    $pathIndex = $_SESSION['path'];

    session_write_close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Latest compiled and minified CSS  -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="pcto.css">
    <link rel="stylesheet" href="/sitoweb/TEMPL/global.css">
</head>
<body>

    <header> <?php include_once $pathIndex . '/TEMPL/HEADER/header.php'; ?> </header>

    <br>
    <br>
    <br>

    <div class="container">
        
        <div class="row blocchi">

            <div class="col-6">
                
                <img src="/sitoweb/IMG/cdjesi_logo.png">
            </div>

            <div class="col-6 ">
                <h2>DOVE?</h2>
                <hr>
                <p>
                    Computer Discount è un piccolo negozio di informatica a Jesi situato sul Viale della Vittoria, in cui viene fornita assistenza su qualsiasi tipo di hardware. 
                </p>
            </div>

        </div>
        
        <br>
        <br>
        
        <div class="row blocchi">

            <div class="col-6 ">
                <h2>QUAL'ERA IL MIO RUOLO?</h2>
                <hr>
                <p>
                    Il negozio è diviso in due zone: la prima è destinata all'accoglienza clienti, in cui sono esposti i vari oggetti elettronici, mentre la seconda zona è destinata ai tecnici, quindi quello è il
                    luogo in cui passavo la maggior parte del mio tempo. Nella mia esperienza lavorativa ho avuto l'opportunità di assemblare diversi computer da 0, riparare stampanti, installare driver necessari
                    per il corretto funzionamento del sistema operativo, sostituzione di componenti danneggiati su laptop (schermi, tastiere, ram, hard-disk). 
                </p>
            </div>

            <div class="col-6">
                <img src="/sitoweb/IMG/elettronica.jpg">
            </div>

        </div>

        <br>
        <br>

        <div class="row blocchi">

            
            <div class="col-12">
                <h2>CONSIDERAZIONI FINALI</h2>
                <hr>
                <p>
                    Devo essere sincero, inizialmente mi sono demoralizzato quando ho scoperto che avrò dovuto svolgere il PCTO in un negozio di elettronica. Essendo un appassionato di programmazione avrei preferito
                    fare quest'esperienza in un'azienda in cui sviluppano software. Ma, nonostante ciò, sono riuscito a far riemergere la mia passione iniziale dei computer e mi sono goduto al massimo questo
                    periodo passato da Computer Discount. Grazie a questa esperienza sono riuscito sia a migliorare le mie skill, sia ad impararne di nuove cercando sempre di trovare una soluzione al problema
                    che mi si poneva di fronte.
                </p>
            </div>

        </div>
         
        <br>
        <br>

        <div class="row blocchi">
            <div class="col">
                <h1>Realizzazione presentazione PCTO in inglese</h1>
                <hr>
            </div>
        </div>

        <br>

        <div class="row blocchi">
            <div class="col">
                <iframe src="/sitoweb/TEMPL/pcto.pdf" style="height: 700px; width: 100%;" frameborder="0"></iframe>
            </div>
        </div>

        <br>
        
    </div>

    <footer> <?php include_once $pathIndex . '/TEMPL/FOOTER/footer.php'; ?> </footer>

    <script>

        var elemento = document.getElementById("PCTO");
        elemento.classList.add("underLine");

    </script>

</body>
</html>