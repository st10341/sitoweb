<?php 
    session_start();

    $pathIndex = $_SESSION['path'];

    session_write_close();
?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Computer</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="computer.css">
    <link rel="stylesheet" href="/sitoweb/TEMPL/global.css">
</head>
<body>
    
    <header> <?PHP include_once $pathIndex . '/TEMPL/HEADER/header.php'; ?> </header>

    <br>
    <br>

    <footer> <?php include_once $pathIndex . '/TEMPL/FOOTER/footer.php'; ?> </footer>

</body>
</html>