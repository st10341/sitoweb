<?php 
    session_start();

    $pathIndex = $_SESSION['path'];

    session_write_close();
?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INFORMATICA</title>

    <!-- Latest compiled and minified CSS  -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/sitoweb/TEMPL/global.css">
    <link rel="stylesheet" href="/sitoweb/PASSIONI/INFORMATICA/informatica.css">
    
</head>
<body>

    <header> <?php include_once $pathIndex . '/TEMPL/HEADER/header.php'; ?> </header>

    <br>
    <br>

    <h1 id="blocchi">Progetti</h1>
    <br>

    <div class="container" id="blocchi">

        <div class="row">

            <div class="me-5 col bordo p-0 contenitore bg-lightyellow">

                <img src="/sitoweb/IMG/tombola.jpg">
                <div class="sfondo-hover">

                    <div class="testo-nascosto">
                        <p id="testo">
                            Come dice il nome stesso ho realizzato un'applicazione desktop per permettere di giocare al classico gioco della tombola insieme ad altri utenti connessi a reti e computer diversi. <br> <br>
                            <span>#C Sharp</span> 
                            <span>#Database</span>
                        </p>
                    </div>

                </div>

            </div>


            <div class="ms-5 col bordo p-0 contenitore bg-blue">

                <img src="/sitoweb/IMG/ChatWave.png">
                <div class="sfondo-hover">

                    <div class="button-centered">
                        <button id="chatwave" onclick="redirectToPage()"></button>
                    </div>

                    <div class="testo-nascosto">
                        <p style="font-weight: bold;">
                            Chat Wave è un'applicazione web in cui si può chattare online tra utenti. Questo progetto include tutte le conoscenze apprese durante i tre anni di indirizzo informatica ed è attualmente il progetto più complesso da me sviluppato. <br> <br>
                            <span id="chat">#HTML #CSS #JS</span> 
                            <span id="chat">#PHP</span> 
                            <span id="chat">#Database</span> 
                            <span id="chat">#Web services</span>
                        </p>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <br>

    <script>
        function showText() 
        {
            var testo = document.querySelector('.testo-nascosto');
            testo.style.opacity = 1;
        }

        function hideText() 
        {
            var testo = document.querySelector('.testo-nascosto');
            testo.style.opacity = 0;
        }

        function redirectToPage() 
        {
            window.open('https://chatwave.sviluppo.host');
        }

    </script>


    <footer> <?php include_once $pathIndex . '/TEMPL/FOOTER/footer.php'; ?> </footer>

</body>
</html>