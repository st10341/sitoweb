<?php 
    session_start();

    $pathIndex = $_SESSION['path'];

    session_write_close();
?>

<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Grafica 3D</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/sitoweb/TEMPL/global.css">
    <link rel="stylesheet" href="grafica.css">
    
    
</head>
<body>
    
    <header> <?PHP include_once $pathIndex . '/TEMPL/HEADER/header.php'; ?> </header>

    <br>
    <br>
    
    <div class="container">
        <div class="row">
            <h1>Blender</h1>
            <hr>
            <h4>Cos'è?</h4>
            <p>
                Blender è un software gratuito e multipiattaforma di modellazione, rigging, animazione, montaggio video, composizione, rendering e texturing di immagini tridimensionali 
                e bidimensionali. Dispone inoltre di funzionalità per mappature UV, simulazioni di fluidi, di rivestimenti, di particelle, altre simulazioni non lineari e creazione 
                di applicazioni/giochi 3D.
            </p>
        </div>

        <br>

        <div class="row">
            <h4>Come ho iniziato?</h4>
            <p>
                Ho iniziato questo percorso per puro caso nel momento in cui mi è capitato tra i video consigliati di Youtube una <a href="https://rb.gy/5zfx70" target="_blank">playlist</a> in cui veniva spiegato il fuzionamento del programma.
                Mi ha sempre attratto questo mondo ma non ho mai avuto il coraggio di iniziare, e questo era il momento perfetto per farlo. <br>
                Grazie a questo tutorial, durato ben 14 video, ho realizzato il mio primo oggetto in 3D, nello specifico una ciambella. A primo impatto sembra banale ma sono stati 
                spiegati molti concetti base per riuscire a capire il funzionamento del programma.
            </p>
        </div>

        <div class="row">
            <h4>Il mio primo render</h4>
            <p>
                Di seguito si può vedere la semplice animazione che ho creato passo passo come veniva spiegato nel video. La fase di rendering è la più importante di tutto il progetto perchè bisogna
                saper impostare bene i punti di luce (solitamente si usa la logica dell'illuminazione a tra punti) per cercare di risaltare il più possibile le zone che si vogliono mettere in evidenza. 
            </p>
            <video src="/sitoweb/TEMPL/donut.mp4" width=100% height=720 controls poster="/sitoweb/TEMPL/donut.png"> </video>

        </div>

        <br>
        <br>
        <br>
        <hr>

        <div class="row">
            <h4>Come ho proseguito?</h4>
            <p>
                Subito dopo il tutorial della ciambella ho iniziato ad interessarmi sempre di più a questo mondo e volevo continuare. Ho trovato un'altra playlist dello stesso Youtuber in cui realizzava
                una sedia. Questo progetto era totalmente diverso dal precedente ed era diviso in più step. <br>
                Innanzi tutto bisognava trovare su internet i cosiddetti 'blueprints' che è sostanzialmente un'immagine in cui viene mostrato l'oggetto sui tre assi (X, Y, Z)
                e, avvolte, vengono indicate le misure precise di ogni componente.
            </p>
        </div>

        <div class="row">
            <div class="col">
                <img src="/sitoweb/IMG/Front.png">
            </div>

            <div class="col">
                <img src="/sitoweb/IMG/Side.png">
            </div>

            <div class="col">
                <img src="/sitoweb/IMG/Top.png">
            </div>
        </div>

        <br>

        <div class="row">
            <h4>Sviluppo della sedia</h4>
            <p>
                Una volta ottenute le immagini guida bisogna crare un progetto vuoto all'interno di Blender, inserire le immagini come background nei rispettivi assi, e iniziare
                a modellare la sedia. <br>
                Lo sviluppo di questo oggetto mi ha impiegato sulle 15 ore complessive e, come per la ciambella, ho fatto un mini render dell'oggetto mentre ruota su se stesso.
            </p>
        </div>

        <br>

        <div class="row">
            <h4>Render finale</h4>
            <p>
                Di seguito si può vedere il render che ho realizzato di questo oggetto, ho deciso di non seguire il tutorial alla lettera e mi sono sbizzarrito per aggiungere qualcosa di mio: ho semplicemente
                aggiunto una pedana in marmo sotto la sedia che si muove su se stessa. 
            </p>
            <video src="/sitoweb/TEMPL/chair.mp4" width=100% height=720 controls poster="/sitoweb/TEMPL/chair.png"> </video>
        </div>

        <br>
        <br>
        <br>
        <hr>

        <div class="row">
            <h4>Progetto personale</h4>
            <p>
                A questo punto mi sentivo pronto per iniziare a realizzare un oggetto 3D da me scelto, ho deciso di modellare un'automobile, nello specifico una Mazda MX-5 versione Roadster. 
                Ormai ho iniziato il progetto da 3/4 mesi ed è ancora in svolgimento. È un oggetto molto complesso da sviluppare avendo molte curve e dettagli complessi da realizzare. <br>
                Il primo step è stato quello di cercare online i blueprints da utilizzare come background nel programma. Dopo molte ore di ricerche sono riuscito a trovarli. 
            </p>
        </div>


        <div class="row">
            <img src="/sitoweb/IMG/mx5_blueprints.png">
        </div>

        <br>
        <br>

        <div class="row">
            <h4>Inizio della modellazione</h4>
            <p>
                Ora è iniziato il progetto vero e proprio. Nei primi mesi ho dedicato molte ore al giorno a sviluppare l'automobile. In questo percorso ho applicato tutte le conoscenze imparate durante i tutorial
                e nonostante ciò, mi sono imbattuto in molti problemi. Per fortuna ho trovato un server discord, della stessa persona che ha realizzato i tutorial, in cui è presente una comunity enorme di appassionati
                di grafica 3D, e c'è sempre qualcuno disponibile per risolvere qualsiasi problema. <br>
                Di seguito ci sono degli screenshot sullo stato attuale dell'automobile.
            </p>
        </div>


        <div class="row">
            <div class="col">
                <img src="/sitoweb/IMG/mx5_front.png">
            </div>

            <div class="col">
                <img src="/sitoweb/IMG/mx5_back.png">
            </div>
            
        </div>

        <br>

        <div class="row">
            <div class="col">
                <img src="/sitoweb/IMG/mx5_lettering.png">
            </div>

            <div class="col">
                <img src="/sitoweb/IMG/mx5_lettering2.png">
            </div>

        </div>

        <br>

        <div class="row">
            <div class="col">
                <img src="/sitoweb/IMG/mx5_logo.png">
            </div>

            <div class="col">
                <img src="/sitoweb/IMG/mx5_rims.png">
            </div>
        </div>

        <br>

        <div class="row">
            <h4>Conclusioni</h4>
            <p>
                In conclusione posso dire di essere estremamente soddisfatto di quello che sono riuscito a creare fino adesso, e spero di finire il progetto entro quest'estate per poi iniziare con qualcosa di nuovo. <br>
                Inoltre vorrei aggiungere che al giorno d'oggi se si vuole veramente imparare qualcosa, grazie ad internet si riesce a raggiungere qualsiasi obiettivo: basta saper cercare nel modo efficiente
                le informazioni ed usarle nel modo migliore possibile.
            </p>
        </div>
        
    </div>
    


    <footer> <?php include_once $pathIndex . '/TEMPL/FOOTER/footer.php'; ?> </footer>

</body>
</html>