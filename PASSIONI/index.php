<?php 
    session_start();

    $pathIndex = $_SESSION['path'];

    session_write_close();
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
        <!-- Latest compiled and minified CSS  -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

        <link rel="stylesheet" href="index.css">
        <link rel="stylesheet" href="/sitoweb/TEMPL/global.css">

    </head>

    <body>

        <header> <?php include_once $pathIndex . '/TEMPL/HEADER/header.php'; ?> </header>
        
        <br>
        <br>
        <br>

        <div class="container">
            <div class="row" id="blocchi">
                
                <div class="col-6">
                    
                    <div class="contenitore">

                        <a href="/sitoweb/PASSIONI/INFORMATICA/informatica.php">
                            <img src="/sitoweb/IMG/programming_language.webp">
                        </a>
                        
                    </div>
                    
                </div>

                <div class="col-6">
                    <h5>INFORMATICA</h5>
                    <hr>
                    <p>
                        Il primo argomento di cui mi sembra doveroso parlare è l'informatica che mi ha dato la possibilità di scoprire un mondo totalmente nuovo e pieno di nuove cose da imparare. 
                        In particolare adoro programmare sia in linguaggi desktop come il C# che linguaggi web come il PHP. 
                        In questa sezione andrò più nel dettaglio su cosa mi hanno permesso di creare i miei tre anni di esperienza scolastica.
                    </p>

                </div>
            </div>

            <br>
            <br>

            <div class="row" id="blocchi">

                <div class="col-6">
                    <h5>COMPUTER</h5>
                    <hr>
                    <p>
                        Questa è la mia seconda passione principale, cioè il mondo hardware dei computer. È iniziato tutto da qui, da quando avevo 13 anni, e mi sono iniziato ad appassionare ai computer.
                        Iniziavo a desiderare sempre di più possedere un computer personale assemblato da me, e alla fine ci sono riuscito.
                        In questa sezione illustrerò da dove sono partito, dove sono arrivato e proverò a immaginere dove arriverò.
                    </p>

                </div>

                <div class="col-6 p-3 text-center contenitore">

                    <div class="contenitore">
                        <a href="/sitoweb/PASSIONI/COMPUTER/computer.php">
                            <img src="/sitoweb/IMG/tech.webp">
                        </a>
                        
                    </div>
                    
                </div>

            </div>

            <br>
            <br>

            <div class="row" id="blocchi">
                
                <div class="col-6 p-3 text-center">

                    <div class="contenitore">
                        <a href="/sitoweb/PASSIONI/GAMING/gaming.php">
                            <img src="/sitoweb/IMG/gaming.jpg">
                        </a>
                        
                    </div>
                    
                </div>

                <div class="col-6">
                    <h5>GAMING</h5>
                    <hr>
                    <p>
                        Altra passione che non poteva assolutamente mancare sono i videogiochi. Ormai sono quasi dieci anni che sono dentro questo mondo e posso dire di conoscerlo abbastanza bene. Come ogni cosa ci sono
                        persone che amano o odiano questo argomento, e ovviamente io sono dal lato buono. Qui spiegherò il mio percorso videoludico illustrando ciò che sono riuscito ad imparare e cercando di spiegare ai 
                        lettori il bello dei videogiochi.
                    </p>

                </div>

            </div>

            <br>
            <br>

            <div class="row" id="blocchi">
                
                <div class="col-6">
                    <h5>GRAFICA 3D</h5>
                    <hr>
                    <p>
                        Infine, da poco tempo, ho iniziato ad appassionarmi di grafica 3D. L'idea iniziale era quella di poter creare qualsiasi cosa mi passasse per la testa. Ho deciso di utilizzare un software molto
                        famoso chiamato Blender. Con questo programma di modellazione 3D l'unico limite è l'immaginazione, ed essendo ancora neofita, sto scoprendo giorno dopo giorno l'immensità di questo programma.
                        In questa sezione illustrerò da dove sono partito, i progetti che ho realizzato e le ambizioni future.
                    </p>

                </div>

                <div class="col-6 p-3 text-center">

                    <div class="contenitore">
                        <a href="/sitoweb/PASSIONI/GRAFICA/grafica.php">
                            <img src="/sitoweb/IMG/blender.png" style="object-fit: contain;">
                        </a>
                    </div>
                    
                </div>

            </div>
        </div>

        <br>
        <br>
        
        <footer> <?php include_once $pathIndex . '/TEMPL/FOOTER/footer.php'; ?> </footer>

        <script>

            var elemento = document.getElementById("PASSIONI");
            elemento.classList.add("underLine");

        </script>

    </body>
    
</html>