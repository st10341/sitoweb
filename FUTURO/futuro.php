<?php 
    session_start();

    $pathIndex = $_SESSION['path'];

    session_write_close();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- Latest compiled and minified CSS  -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="futuro.css">
    <link rel="stylesheet" href="/sitoweb/TEMPL/global.css">

</head>
<body>
    
    <header> <?PHP include_once $pathIndex . '/TEMPL/HEADER/header.php'; ?> </header>

    <br>
    <br>
    <br>



    <div class="container">
        <div class="row">

            <div class="col">
                <h1 style="text-align: center;">FUTURO</h1>
            </div>

        </div>

        <br>

        <div class="row">

            <div class="col">
                <p style="text-align: justify;">
                    Eccoci arrivati alla parte finale del mio blog, in cui ho parlato di me stesso, delle mie passioni, del mio percorso scolastico e dele mie esperienze lavorative. <br>
                    Alla fine di tutto l'informatica sarà l'ambito che mi porterò dietro per tutta la vita, ed essendo una materia in continuo sviluppo bisogna rimanere sempre aggiornati sulle ultime novità.
                    Nella sezione delle mie passioni ho parlato anche della Grafica 3D e di come mi sta appassionando, e magari in futuro potrei abbandonare del tutto l'informatica e buttarmi in questo nuovo mondo.
                    Ovviamente se sono fortunato potrò usare entrambe le cose insieme dato che nella progettazione di videogiochi c'è sia la parte di programmazione e sia la parte di realizzazione dei personaggi. <br>
                    In conclusione adoro la tecnologia e qualsiasi cosa legata al mondo dei computer e spero di riuscire a realizzare tutti i miei sogni.
                </p>
            </div>

        </div>
    </div>







    <footer> <?php include_once $pathIndex . '/TEMPL/FOOTER/footer.php'; ?> </footer>

    <script>

        var elemento = document.getElementById("FUTURO");
        elemento.classList.add("underLine");

    </script>

</body>
</html>