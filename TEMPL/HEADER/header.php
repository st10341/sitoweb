<!DOCTYPE html>
<html lang="it">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Il tuo titolo</title>
        <link rel="stylesheet" href="/sitoweb/TEMPL/HEADER/header.css">
    </head>

    <body>

        <header class="header">
            <nav>
                <ul>
                    <li><a href="/sitoweb/index.php" id="HOME"><p>Home Page</p></a></li>
                    <li><a href="/sitoweb/PCTO/pcto.php" id="PCTO"><p>PCTO</p></a></li>
                    <li><a href="/sitoweb/PASSIONI/index.php" id="PASSIONI"><p>Passioni</p></a></li>
                    <li><a href="/sitoweb/SCUOLA/scuola.php" id="PS"><p>Percorso scolastico</p></a></li>
                    <li><a href="/sitoweb/FUTURO/futuro.php" id="FUTURO"><p>Futuro</p></a></li>
                </ul>
            </nav>
        </header>

    </body>
    
</html>
