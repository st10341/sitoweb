<link rel="stylesheet" href="/sitoweb/TEMPL/FOOTER/footer.css">


<div id="footer">

    <div class="row m-0 text-center">
        <div class="col">
            <p>Contatti</p>                
            <hr>
            <p>
                <strong>
                    E-mail personale
                </strong>
                <br>
                mauriziodellopreite11@gmai.com
            </p>

            <br>

            <p>
                <strong>
                    E-mail scolastica
                </strong>
                <br>
                st10341@iismarconipieralisi.it
            </p>
            
        </div>

        <div class="col">
            <p>Collegamenti</p>
            <hr>
            <p>
                <strong>
                    La mia scuola
                </strong>
                <br>
                <a href="https://www.iismarconipieralisi.edu.it/" target="_blank">IIS Marconi Pieralisi</a>
                <br>
                <br>
                <strong>Progetti</strong>
                <br>
                <a href="https://chatwave.sviluppo.host/webchat/index.php" target="_blank">Chat Wave</a>
                
            </p>
        </div>

        <div class="col">
            <p>Todo...</p>
            <hr>
        </div>
    </div>
        
    <hr style="margin-bottom: 10px;">
    <p class="text-center">Tutti i diritti riservati</p>

</div>